#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

set -e

# Install

    aptitude -y install zsh

# Enhance Zsh with Oh-my-Zsh

    # screen must be used to install oh-my-zsh, otherwise we will get stuck in zsh shell after installation is complete
    screen -d -m sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


# Tracking changes

    sleep 10 && # 10 seconds should be more than enough to the installation have finished
    cd /etc &&
    git status &&
    git add --all &&
    git commit -m 'After ZSH have been installed and enhanced with oh-my-zsh.'
