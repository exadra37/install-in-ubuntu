#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

set -e

# Install

    aptitude -y install git

# Setup

    git config --global user.email "exadra37@gmail.com"
    git config --global user.name "Exadra37"
