#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

set -e

## Setup

    # line  5: Port 8095
    # changing here the default port for SSH, implies also to open it in the firewall
    sed -i 's|Port 22|Port 8095|g' /etc/ssh/sshd_config

    # line 28: PermitRootLogin no
    sed -i 's|PermitRootLogin yes|PermitRootLogin no|g' /etc/ssh/sshd_config

    # line 52: PasswordAuthentication no
    sed -i 's|#PasswordAuthentication yes|PasswordAuthentication no|g' /etc/ssh/sshd_config

    # Line 64 - X11Forwarding no
    sed -i 's|X11Forwarding yes|X11Forwarding no|g' /etc/ssh/sshd_config

    # http://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html

        printf "\n#http://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html" | sudo tee -a /etc/ssh/sshd_config

        # Disable TCP fowarding
        echo 'AllowTcpForwarding no # By Exadra37' | sudo tee -a /etc/ssh/sshd_config

        # Turn on  reverse name checking
        echo 'VerifyReverseMapping yes # By Exadra37' | sudo tee -a /etc/ssh/sshd_config

        # Turn on  reverse name checking
        echo 'AllowUsers exadra37 # By Exadra37' | sudo tee -a /etc/ssh/sshd_config

    # listen only in IPV4
    echo 'AddressFamily inet # By Exadra37' | sudo tee -a /etc/ssh/sshd_config

    service ssh restart


## Create User

    # Once root user have now direct login disabled, we need another user
    #  to ssh into the server

    adduser exadra37 &&
    mv /root/.ssh /home/exadra37 && # root user have login disabled, using is public key
    chown -R exadra37:exadra37 /home/exadra37/.ssh


# Tracking changes
    cd /etc &&
    git status &&
    git commit -am 'After SSH have been configured and a new user created.'
