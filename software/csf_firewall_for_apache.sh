#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/04/01
# @link exadra37.com

set -e

## Setup

    # TCP_IN

        # Line 139 - allowed incoming Tcp ports
        sed -i 's|TCP_IN = "20,21,22,25,53,80,110,143,443,465,587,993,995"|TCP_IN = "20,21,80,443,8095"|g' /etc/csf/csf.conf


    # TCP_OUT

        # Line 142 - allowed Tcp outgoing ports
        sed -i 's|TCP_OUT = "20,21,22,25,53,80,110,143,443,465,587,993,995"|TCP_OUT = "20,21,80,443,8095"|g' /etc/csf/csf.conf

    # UDP_IN

        # Line 145 - allowed Udp incoming ports
        sed -i 's|UDP_IN = "20,21,53"|UDP_IN = "20,21"|g' /etc/csf/csf.conf


    # UDP_OUT

        # Line 149 - allowed Udp outgoing ports
        sed -i 's|UDP_OUT = "20,21,53,113,123"|UDP_OUT = "20,21,123"|g' /etc/csf/csf.conf


    # TCP6_IN

        # Line 225 - allowed incoming Tcp ports
        sed -i 's|TCP6_IN = "20,21,22,25,53,80,110,143,443,465,587,993,995"|TCP6_IN = "20,21,80,443,8095"|g' /etc/csf/csf.conf


    # TCP6_OUT

        # Line 228 - allowed Tcp outgoing ports
        sed -i 's|TCP6_OUT = "20,21,22,25,53,80,110,143,443,465,587,993,995"|TCP6_OUT = "20,21,80,443,8095"|g' /etc/csf/csf.conf

    # UDP6_IN

        # Line 231 - allowed Udp incoming ports
        sed -i 's|UDP6_IN = "20,21,53"|UDP6_IN = "20,21"|g' /etc/csf/csf.conf


    # UDP6_OUT

        # Line 235 - allowed Udp outgoing ports
        sed -i 's|UDP6_OUT = "20,21,53,113,123"|UDP6_OUT = "20,21,123"|g' /etc/csf/csf.conf


    # SYNFLOOD

        # Line 457 - Enable Syn Flood protection
        # when enabled it will impact server performance
        sed -i 's|SYNFLOOD = "0"|SYNFLOOD = "1"|g' /etc/csf/csf.conf

        # Line 458 - adjust Syn Flood Burst
        sed -i 's|SYNFLOOD_RATE = "100/s"|SYNFLOOD_RATE = "120/s"|g' /etc/csf/csf.conf

        # Line 459 - adjust Syn Flood Burst
        sed -i 's|SYNFLOOD_BURST = "150"|SYNFLOOD_BURST = "3"|g' /etc/csf/csf.conf

        # After applying the above the config should look like:
        #   SYNFLOOD = "1"
        #   SYNFLOOD_RATE = "120/s"
        #   SYNFLOOD_BURST = "3"
        #
        # The above means that if 120 connections per second happens more than 3 times,
        #  from the same IP, this IP will be blocked.


    # UDPFLOOD

        # Line 502 - enable UDPFLOOD
        sed -i 's|UDPFLOOD = "0"|UDPFLOOD = "1"|g' /etc/csf/csf.conf


    # CONNLIMIT

        # Line 477 - Adjust connections limit by port
        # For example port 80 will only accept until 100 concurrent connections per ip address,
        #  otherwise offended ip address will be blocked.
        sed -i 's|CONNLIMIT = ""|CONNLIMIT = "8095;4,80;100,443;100"|g' /etc/csf/csf.conf

        # Line 563 - Enable logging of dropped connections
        sed -i 's|CONNLIMIT_LOGGING = "0"|CONNLIMIT_LOGGING = "1"|g' /etc/csf/csf.conf


    # PORTFLOOD

        # Line 493 - Adjust connections limit by time interval to a specific port and
        # As per man page, by default it only counts 20 packets per address remembered,
        #  this means that you need to keep the hit count to below 20.
        # For example port 8095 will only accept 4 connections by tcp within 86400, otherwise
        #  offended ip address will be blocked and a calm period of 86400 must be observed
        #  for the block be lifted
        sed -i 's|PORTFLOOD = ""|PORTFLOOD = "8095;tcp;4;86400"|g' /etc/csf/csf.conf


    # LF_ALERT_TO

        # Line 588 - Set email to be used in alerts
        sed -i 's|LF_ALERT_TO = ""|LF_ALERT_TO = "kfoxtrot37@gmail.com"|g' /etc/csf/csf.conf

    # LF_DISTFTP

        # Line 1267 - set how many login for FTP can be logged at same time for same user
        # If more than 3 logins detected, than all ip addresses will be blocked
        sed -i 's|LF_DISTFTP = "0"|LF_DISTFTP = "3"|g' /etc/csf/csf.conf


    # UID_INTERVAL

        # Line 1649 - enable User Id Tracking
        # Check every 60 seconds for Users Ids associated to blocks in syslog
        sed -i 's|UID_INTERVAL = "0"|UID_INTERVAL = "60"|g' /etc/csf/csf.conf


    # reload firewall
    csf -r
    service lfd restart

    # Tracking changes
    cd /etc &&
    git status &&
    git commit -am 'After CSF have been configured for Apache.'

