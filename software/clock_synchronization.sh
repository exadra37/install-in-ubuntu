#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

set -e


# Set Time Zone

    dpkg-reconfigure tzdata


# Clock synchronization

    aptitude -y install ntp


# Tracking changes
    cd /etc &&
    git status &&
    git add --all &&
    git commit -m 'After Time Zone been set and NTP have been installed.'
