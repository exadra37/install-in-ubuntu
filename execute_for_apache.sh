#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

cd ~ &&
mkdir setup &&
wget https://gitlab.com/exadra37/install-in-ubuntu/repository/archive.tar.gz &&
tar zxvf archive.tar.gz -C setup --strip-components=1 &&
./setup/1404-lts/server/setup_for_apache.sh

rm -rvf archive.tar.gz
rm -rvf setup

ls -al

# now e can go for zsh shell
zsh
