#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/28
# @link exadra37.com

set -e

# Set the script dir

    script_dir=$( cd "$( dirname "$0" )" && pwd )


# Update

    aptitude update


# Install and Setup Git

    "$script_dir/../../software/git.sh"


# Start Tracking with Git folder /etc

    "$script_dir/../../tasks/tracking_etc.sh"


# Install and Setup CSF firewall

    "$script_dir/../../software/csf_firewall.sh"


# Clock Synchronization

    "$script_dir/../../software/clock_synchronization.sh"


# SSH

    "$script_dir/../../software/ssh.sh"
