#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/28
# @link exadra37.com


# Set the script dir

    script_dir=$( cd "$( dirname "$0" )" && pwd )


# Start setup

    "$script_dir/setup_start.sh"


# Specific setup for Apache

    "$script_dir/../../software/csf_firewall_for_apache.sh"


# End setup

    "$script_dir/setup_end.sh"
