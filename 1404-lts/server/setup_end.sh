#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/28
# @link exadra37.com

set -e

# Set the script dir

    script_dir=$( cd "$( dirname "$0" )" && pwd )


# Keep system up to date

    aptitude -y upgrade

    cd /etc &&
    git status &&
    git add --all &&
    git commit -m 'Keep system up to date with "aptitude upgrade".'


# Root Password

    printf "\n>>> SET PASSWORD FOR ROOT USER <<<\n"
    passwd


# ZSH

    "$script_dir/../../software/zsh.sh"
