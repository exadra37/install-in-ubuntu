# Install in Ubuntu

## Setup

Using CURL in command line:

```bash
sh -c "$(curl -fsSL https://gitlab.com/exadra37/install-in-ubuntu/raw/master/execute_for_apache.sh)"
```

Using WGET in command line:

```bash
sh -c "$(wget  https://gitlab.com/exadra37/install-in-ubuntu/raw/master/execute_for_apache.sh -O -)"
```
