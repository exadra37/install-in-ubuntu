#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2016/03/31
# @link exadra37.com

set -e

## Tracking

    cd /etc

    git init

    git add --all

    git commit -m 'Big Bang :)'
